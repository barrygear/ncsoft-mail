#!/bin/bash
dotnet restore
dotnet build -r centos.7-x64
dotnet publish -c Release -r centos.7-x64

zip -j NCSoft-Mail-centos.7-x64.zip bin/Release/netcoreapp1.1/centos.7-x64/publish/*
