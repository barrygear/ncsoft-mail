NCSOFT Server Engineering Code Sample

Create a RESTful mail service in C#. Specifications are included below from the perspective of a feature request from a game team.  Be encouraged to reach out with any questions or clarifications.

Specifications

* We should not need to install any software or libraries in order to run this service. Engineers, designers and artists need to be able to run this locally.
* Detailed API documentation on how to use this service.
* We should be able to send mail to other users with the information:
* * Subject
* * Message
* Support group mail and one-on-one mail.
* We should be able to mark mail as read or unread.
* Should be able to delete mail.

Possible Improvements

Items from this list are not required but will help demonstrate your expertise.  Senior Engineering candidates are encouraged to complete at least one improvement. Feel free to implement features of your own creation. If you do decide to implement your own feature, be ready to explain why it was important.

* Unit tests.
* Persistence abstraction.
* Mail headers in addition to mail messages.
* Socket interface for push messages. 
* Load testing client.

Evaluation

We will be evaluating your test based upon the following merits:

* Are all specifications met?
* Is the deliverable professional and polished?
* Does the application demonstrate acceptable performance?
* Does the approach imply relevant technical expertise?
* Are the employed algorithms/structures efficient?
* Is the project well organized?
* Is the coding style clear and consistent?
* Is the code structured to be simple, maintainable and extendable?

Deliverables

In a zipped archive, provide the following:

* Executable
* Source code
* Description of implementations, assumptions, and external dependencies

