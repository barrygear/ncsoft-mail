#!/bin/bash
dotnet restore
dotnet build -r debian.8-x64
dotnet publish -c Release -r debian.8-x64

zip -j NCSoft-Mail-debian.8-x64.zip bin/Release/netcoreapp1.1/debian.8-x64/publish/*
