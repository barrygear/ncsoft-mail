## MailUser - GetAll
curl -X "GET" "http://localhost:5000/api/MailUser"

## MailUser - Create User
curl -X "POST" "http://localhost:5000/api/MailUser" \
     -H "Content-Type: application/json; charset=utf-8" \
     -d "{\"Name\":\"Barry Gear\",\"Recipient\":\"barrygear@gmail.com\"}"

## MailUser - GetByName
curl -X "GET" "http://localhost:5000/api/MailUser/Barry%20Gear/name"

## MailUser - GetByRecipient
curl -X "GET" "http://localhost:5000/api/MailUser/barrygear@gmail.com/recipient"

## Groups - GetAllGroups
curl -X "GET" "http://localhost:5000/api/Group"

## Groups - DeleteByKey
curl -X "DELETE" "http://localhost:5000/api/Group/a252949a-540f-4455-a5dc-43241025a233"

## Groups - CreateGroup
curl -X "POST" "http://localhost:5000/api/Group" \
     -H "Content-Type: application/json; charset=utf-8" \
     -d "{\"Name\":\"Engineering\",\"Recipient\":\"barrygear@gmail.com\"}"

## Groups - CreateGroup FAIL
curl -X "POST" "http://localhost:5000/api/Group" \
     -H "Content-Type: application/json; charset=utf-8" \
     -d "{\"Name\":\"Engineering\",\"Recipient\":\"test@gmail.com\"}"

## Mail - GetAllMail
curl -X "GET" "http://localhost:5000/api/Mail"

## Mail - FindAllBySender
curl -X "GET" "http://localhost:5000/api/Mail/barrygear@gmail.com/sender"

## Mail - FindAllByRecipient
curl -X "GET" "http://localhost:5000/api/Mail/barrygear@gmail.com/all"

## Mail - FindAllReadByRecipient
curl -X "GET" "http://localhost:5000/api/Mail/test@gmail.com/read"

## Mail - FindAllUnreadByRecipient Duplicate
curl -X "GET" "http://localhost:5000/api/Mail/test@gmail.com/unread"

## Mail - Create Mail
curl -X "POST" "http://localhost:5000/api/Mail" \
     -H "Content-Type: application/json; charset=utf-8" \
     -d "{\"Sender\":\"barrygear@gmail.com\",\"Message\":\"Group Test3\",\"Recipient\":\"Engineering\",\"Subject\":\"Group Test3\",\"Message\":\"test\"}"

## Mail - Mark Read
curl -X "PATCH" "http://localhost:5000/api/Mail/188b1652-ba6b-493c-a0af-b52cdc5a695b" \
     -H "Content-Type: application/json; charset=utf-8" \
     -d "{\"Read\":true}"

## Mail - Replace Mail
curl -X "PUT" "http://localhost:5000/api/Mail/99c4f806-86e1-4a84-b98c-e2500f1f3c27" \
     -H "Content-Type: application/json; charset=utf-8" \
     -d "{\"Sender\":\"barrygear@gmail.com\",\"Message\":\"Hello!\",\"Recipient\":\"test@gmail.com\",\"Subject\":\"Hello\",\"Key\":\"99c4f806-86e1-4a84-b98c-e2500f1f3c27\"}"

## Mail - Mark Unread
curl -X "PATCH" "http://localhost:5000/api/Mail/29a7259b-813f-4ccc-8f4c-b9eb61560d61" \
     -H "Content-Type: application/json; charset=utf-8" \
     -d "{\"Read\":false}"
