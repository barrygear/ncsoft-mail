#!/bin/bash
dotnet restore
dotnet build -r osx.10.12-x64
dotnet publish -c Release -r osx.10.12-x64

zip -j NCSoft-Mail-osx-10.12-x64.zip bin/Release/netcoreapp1.1/osx.10.12-x64/publish/*
