/*
 * Project: NCSoft-Mail
 * File: src/Test/MailTest.cs
 */
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Mail;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Xunit;
using Xunit.Abstractions;

namespace MailTests
{
    public class MailTest
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;
        private readonly ITestOutputHelper output;

        private string request = "/api/Mail/";
            
        public MailTest(ITestOutputHelper output)
        {
            var host = new WebHostBuilder()
                       .UseKestrel()
                       .UseIISIntegration()
                       .UseStartup<TestStartup>();

            _server = new TestServer(host);

            _client = _server.CreateClient();

            this.output = output;
        }

        [Theory]
        [InlineDataAttribute()]
        private async Task<string> GetMailResponse(string querystring = "")
        {
            var response = await _client.GetAsync(request);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        [Fact]
        public async Task ReturnGetAllMail()
        {
            // Act
            var response = await _client.GetAsync(request);
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            // Assert
            Assert.NotNull(responseString);
            output.WriteLine("Received {0}", responseString);
            
        }

        // [Theory]
        // [InlineDataAttribute(1)]
        // public async Task ReturnGetAllMail(int count)
        // {
        //     // Act
        //     var response = await _client.GetAsync(request);
        //     response.EnsureSuccessStatusCode();

        //     var responseString = await response.Content.ReadAsStringAsync();
        //     // Assert
        //     Assert.NotNull(responseString);
        //     output.WriteLine("Received {0}", responseString);
        //     List<MailItem> items = JsonConvert.DeserializeObject<List<MailItem>>(responseString);
        //     Assert.True(items.Count.Equals(count));
        // }

        [Fact]
        public async Task ReturnFindAllBySender()
        {
            // Act
            string sender = "Sender1";
            var response = await _client.GetAsync(request + sender + "/sender");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            // Assert
            Assert.NotNull(responseString);
            output.WriteLine("Received {0}", responseString);
        }

        [Fact]
        public async Task SendMessageNoSender()
        {
            // Act
            var httpRequestContent = 
                new StringContent("{ \"Meh\": \"barrygear@gmail.com\"}", Encoding.UTF8, "application/json");
            var response = await _client.PostAsync(request, httpRequestContent);
            output.WriteLine("Received {0}", response);
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
           var responseString = await response.Content.ReadAsStringAsync();
            // Assert
            Assert.NotNull(responseString);
            Assert.Equal("Missing Sender", responseString);
            output.WriteLine("Received {0}", responseString);
        }
    }
}