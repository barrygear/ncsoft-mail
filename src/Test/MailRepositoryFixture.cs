/*
 * Project: NCSoft-Mail
 * File: src/Test/MailRepositoryFixture.cs
 */
using System;
using Mail;

public class MailRepositoryFixture : IDisposable
{
    public MailRepositoryFixture()
    {
        MailRespository = new MailRepository_Mem();

        // ... initialize data in the test database ...
    }

    public void Dispose()
    {
        // ... clean up test data from the database ...
    }

    public IMailRepository MailRespository { get; private set; }
}