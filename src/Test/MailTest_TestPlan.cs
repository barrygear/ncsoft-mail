/*
 * Project: NCSoft-Mail
 * File: src/Test/MailTest.cs
 */
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Mail;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace MailTests
{
    [TestCaseOrderer("MailTests.PriorityOrderer", "NCSoft-Mail")]
    public class MailTest_TestPlan
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;
        private readonly ITestOutputHelper output;
        private const string request = "/api/Mail/";
            
        public MailTest_TestPlan(ITestOutputHelper output)
        {
            var host = new WebHostBuilder()
                       .UseKestrel()
                       .UseIISIntegration()
                       .UseStartup<TestStartup>();

            _server = new TestServer(host);

            _client = _server.CreateClient();

            this.output = output;
        }

        [Theory, TestPriority(0)]
        [InlineDataAttribute("TestUser1", "TestRecipient1")]
        [InlineDataAttribute("TestUser2", "TestRecipient2")]
        public async Task ClearMail(string name, string recipient)
        {
            MailUser mailUser = new MailUser(name, recipient);
            
            // Check for empty mail box
            var response = await _client.GetAsync(request + recipient + "/all");
            var responseString = await response.Content.ReadAsStringAsync();
            if (responseString != null) {
                List<MailItem> mailItems = JsonConvert.DeserializeObject<List<MailItem>>(responseString);
                if(mailItems != null) {
                    foreach(MailItem mailItem in mailItems) {
                        response = await _client.DeleteAsync(request + mailItem.Key);                    
                    }
                }
            }
        }

        [Theory, TestPriority(1)]
        [InlineDataAttribute("GroupName")]
        public async Task ClearGroup(string groupName)
        {
            const string groupRequest = "/api/Group/";

            // Ensure group is greated with at least 1 member
            
            var response = await _client.GetAsync(groupRequest + groupName + "/name");
            var responseString = await response.Content.ReadAsStringAsync();
            if(response.StatusCode == HttpStatusCode.OK) {
                List<GroupMember> groupMembers = JsonConvert.DeserializeObject<List<GroupMember>>(responseString);
                if(groupMembers.Count > 0) {
                    foreach(GroupMember groupMember in groupMembers) {
                        response = await _client.DeleteAsync(groupRequest + groupMember.Key);
                    }
                }
            }
        }

        // [Theory, TestPriority(2)]
        // [InlineDataAttribute("TestUser1", "TestRecipient1")]
        // [InlineDataAttribute("TestUser2", "TestRecipient2")]
        // public async Task CreateUser(string name, string recipient)
        // {
        //     // Posting creates a user
        //     MailUser mailUser = new MailUser(name, recipient);  
        //     var request = "/api/MailUser";
        //     var httpRequestContent = new StringContent(JsonConvert.SerializeObject(mailUser), Encoding.UTF8, "application/json");
        //     var response = await _client.PostAsync(request, httpRequestContent);
        //     output.WriteLine("Received {0}", response);
        //     response.EnsureSuccessStatusCode();

        //     var responseString = await response.Content.ReadAsStringAsync();
        //     Assert.NotNull(responseString);
        //     output.WriteLine("Received {0}", responseString);
        // }

        [Theory, TestPriority(3)]
        [InlineDataAttribute("TestUser1", "TestRecipient1", "TestSubject1", "TestMessage1")]
        [InlineDataAttribute("TestUser2", "TestRecipient2", "TestSubject2", "TestMessage2")]
        public async Task TestMail(string name, string recipient, string subject, string message)
        {
            // Posting creates a user
            MailUser mailUser = new MailUser(name, recipient);  
            var userRequest = "/api/MailUser";
            var httpRequestContent = new StringContent(JsonConvert.SerializeObject(mailUser), Encoding.UTF8, "application/json");
            var response = await _client.PostAsync(userRequest, httpRequestContent);
            output.WriteLine("Received {0}", response);
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            output.WriteLine("Received {0}", responseString);

            // Check for empty mail box
            response = await _client.GetAsync(request + recipient + "/all");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

            List<MailItem> mailItems;
            // Send Mail to self
            MailItem mailItem = new MailItem(name, recipient, subject, message);
            httpRequestContent =
                new StringContent(JsonConvert.SerializeObject(mailItem), Encoding.UTF8, "application/json");
            response = await _client.PostAsync(request, httpRequestContent);
            output.WriteLine("Received {0}", response);
            response.EnsureSuccessStatusCode();
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            MailItem sentItem = JsonConvert.DeserializeObject<MailItem>(responseString);
            Assert.NotNull(mailItem);
            output.WriteLine("Received {0}", responseString);

            // Check Read Mail, should be empty as it hasnt read any yet
            response = await _client.GetAsync(request + recipient + "/read");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

            // Check Unead Mail, should be 1 as it hasnt read any yet
            response = await _client.GetAsync(request + recipient + "/unread");
            response.EnsureSuccessStatusCode();
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            mailItems = JsonConvert.DeserializeObject<List<MailItem>>(responseString);
            Assert.NotNull(mailItems);
            Assert.True(mailItems.Count == 1);

            // Mail message as read
            mailItem = new MailItem();
            mailItem.Read = true;
            string key = mailItems[0].Key;
            httpRequestContent =
                new StringContent(JsonConvert.SerializeObject(mailItem), Encoding.UTF8, "application/json");
            response = await _client.PatchAsync(request+ key, httpRequestContent);
            response.EnsureSuccessStatusCode();

            // Check Read Mail, should be 1
            response = await _client.GetAsync(request + recipient + "/read");
            response.EnsureSuccessStatusCode();
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            mailItems = JsonConvert.DeserializeObject<List<MailItem>>(responseString);
            Assert.NotNull(mailItems);
            Assert.True(mailItems.Count == 1);

            // Check Unread Mail, should be 0
            response = await _client.GetAsync(request + recipient + "/unread");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

            // Delete MailItems
            response = await _client.DeleteAsync(request + key);
            response.EnsureSuccessStatusCode();

            // Check all Mail, should be 0
            response = await _client.GetAsync(request + recipient + "/all");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Theory, TestPriority(4)]
        [InlineDataAttribute("TestSender1", "TestRecipient1", "TestSender2", "TestRecipient2", "TestSubject1", "TestMessage1")]
        public async Task TestMailTwoUsers(string sender1, string recipient1, string sender2, string recipient2, string subject, string message)
        {
            // Posting creates a user
            MailUser mailUser = new MailUser(sender1, recipient1);  
            var userRequest = "/api/MailUser";
            var httpRequestContent = new StringContent(JsonConvert.SerializeObject(mailUser), Encoding.UTF8, "application/json");
            var response = await _client.PostAsync(userRequest, httpRequestContent);
            output.WriteLine("Received {0}", response);
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            output.WriteLine("Received {0}", responseString);

            // Posting creates a user
            mailUser = new MailUser(sender2, recipient2);  
            httpRequestContent = new StringContent(JsonConvert.SerializeObject(mailUser), Encoding.UTF8, "application/json");
            response = await _client.PostAsync(userRequest, httpRequestContent);
            output.WriteLine("Received {0}", response);
            response.EnsureSuccessStatusCode();

            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            output.WriteLine("Received {0}", responseString);

            List<MailItem> mailItems;
            
            // Check Read Mail for recipient, should be empty as it hasnt read any yet
            response = await _client.GetAsync(request + recipient2 + "/all");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

            // Send Mail to user 1
            MailItem mailItem = new MailItem(sender1, recipient2, subject, message);
            httpRequestContent =
                new StringContent(JsonConvert.SerializeObject(mailItem), Encoding.UTF8, "application/json");
            response = await _client.PostAsync(request, httpRequestContent);
            output.WriteLine("Received {0}", response);
            response.EnsureSuccessStatusCode();
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            MailItem sentItem = JsonConvert.DeserializeObject<MailItem>(responseString);
            Assert.NotNull(mailItem);
            output.WriteLine("Received {0}", responseString);

            // Check Read Mail, should be empty as it hasnt read any yet
            response = await _client.GetAsync(request + sender1 + "/read");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

            // Check Unread Mail, should be 1
            response = await _client.GetAsync(request + recipient2 + "/unread");
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            mailItems = JsonConvert.DeserializeObject<List<MailItem>>(responseString);
            Assert.NotNull(mailItems);
            Assert.True(mailItems.Count == 1);

            // Mail message as read
            mailItem = new MailItem();
            mailItem.Read = true;
            string key = mailItems[0].Key;
            httpRequestContent =
                new StringContent(JsonConvert.SerializeObject(mailItem), Encoding.UTF8, "application/json");
            response = await _client.PatchAsync(request+ key, httpRequestContent);
            response.EnsureSuccessStatusCode();

            // Check Read Mail, should be 1
            response = await _client.GetAsync(request + recipient2 + "/read");
            response.EnsureSuccessStatusCode();
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            mailItems = JsonConvert.DeserializeObject<List<MailItem>>(responseString);
            Assert.NotNull(mailItems);
            Assert.True(mailItems.Count == 1);

            // Check Unead Mail, should be 0
            response = await _client.GetAsync(request + recipient2 + "/unread");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

            // Delete MailItems
            response = await _client.DeleteAsync(request + key);
            response.EnsureSuccessStatusCode();

            // Check Unead Mail, should be 0
            response = await _client.GetAsync(request + recipient2 + "/all");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Theory, TestPriority(5)]
        [InlineDataAttribute("TestSender1", "TestRecipient1", "TestSender2", "TestRecipient2", "GroupName", "TestSubject1", "TestMessage1")]
        public async Task TestMailForGroups(
            string sender1, 
            string recipient1, 
            string sender2, 
            string recipient2, 
            string groupName, 
            string subject, 
            string message)
        {

            // Posting creates a user
            MailUser mailUser = new MailUser(sender1, recipient1);  
            var userRequest = "/api/MailUser";
            var httpRequestContent = new StringContent(JsonConvert.SerializeObject(mailUser), Encoding.UTF8, "application/json");
            var response = await _client.PostAsync(userRequest, httpRequestContent);
            output.WriteLine("Received {0}", response);
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            output.WriteLine("Received {0}", responseString);

            // Posting creates a user
            mailUser = new MailUser(sender2, recipient2);  
            httpRequestContent = new StringContent(JsonConvert.SerializeObject(mailUser), Encoding.UTF8, "application/json");
            response = await _client.PostAsync(userRequest, httpRequestContent);
            output.WriteLine("Received {0}", response);
            response.EnsureSuccessStatusCode();

            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            output.WriteLine("Received {0}", responseString);

            const string groupRequest = "/api/Group/";

            List<MailItem> mailItems;
            
            // Ensure group is greated with at least 1 member
            GroupMember groupMember = new GroupMember(groupName, recipient1);
            httpRequestContent = new StringContent(
                JsonConvert.SerializeObject(groupMember), 
                Encoding.UTF8, 
                "application/json");
            response = await _client.PostAsync(groupRequest, httpRequestContent);
            response.EnsureSuccessStatusCode();
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            GroupMember createdGroup = JsonConvert.DeserializeObject<GroupMember>(responseString);
            Assert.NotNull(createdGroup);
            output.WriteLine("Received {0}", responseString);

            // 2nd member joins group
            groupMember = new GroupMember(groupName, recipient2);
            httpRequestContent = new StringContent(
                JsonConvert.SerializeObject(groupMember), 
                Encoding.UTF8, 
                "application/json");
            response = await _client.PostAsync(groupRequest, httpRequestContent);
            response.EnsureSuccessStatusCode();
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            createdGroup = JsonConvert.DeserializeObject<GroupMember>(responseString);
            Assert.NotNull(createdGroup);
            output.WriteLine("Received {0}", responseString);

            // Get Group Members
            response = await _client.GetAsync(groupRequest + groupName + "/name");
            response.EnsureSuccessStatusCode();
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            List<GroupMember> groupMembers = JsonConvert.DeserializeObject<List<GroupMember>>(responseString);
            Assert.NotNull(createdGroup);
            Assert.True(groupMembers.Count == 2);
            output.WriteLine("Received {0}", responseString);

            // Send Mail to Group
            MailItem mailItem = new MailItem(recipient1, groupName, subject, message);
            httpRequestContent =
                new StringContent(JsonConvert.SerializeObject(mailItem), Encoding.UTF8, "application/json");
            response = await _client.PostAsync(request, httpRequestContent);
            output.WriteLine("Received {0}", response);
            response.EnsureSuccessStatusCode();
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            MailItem sentItem = JsonConvert.DeserializeObject<MailItem>(responseString);
            Assert.NotNull(mailItem);
            output.WriteLine("Received {0}", responseString);

            // Check Unread Mail for user 1, should be 1
            response = await _client.GetAsync(request + recipient1 + "/unread");
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            mailItems = JsonConvert.DeserializeObject<List<MailItem>>(responseString);
            Assert.NotNull(mailItems);
            Assert.True(mailItems.Count == 1);

            // Check Unread Mail for user 2, should be 1
            response = await _client.GetAsync(request + recipient2 + "/unread");
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            mailItems = JsonConvert.DeserializeObject<List<MailItem>>(responseString);
            Assert.NotNull(mailItems);
            Assert.True(mailItems.Count == 1);

            // Mail message as read for user 2
            mailItem = new MailItem();
            mailItem.Read = true;
            string key = mailItems[0].Key;
            httpRequestContent =
                new StringContent(JsonConvert.SerializeObject(mailItem), Encoding.UTF8, "application/json");
            response = await _client.PatchAsync(request+ key, httpRequestContent);
            response.EnsureSuccessStatusCode();

            // Check Unread Mail for user 2, should be 0
            response = await _client.GetAsync(request + recipient2 + "/unread");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

            // Check Unread Mail for user 1, should be 1
            response = await _client.GetAsync(request + recipient1 + "/unread");
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            mailItems = JsonConvert.DeserializeObject<List<MailItem>>(responseString);
            Assert.NotNull(mailItems);
            Assert.True(mailItems.Count == 1);

            // Mail message as read for user 1
            mailItem = new MailItem();
            mailItem.Read = true;
            key = mailItems[0].Key;
            httpRequestContent =
                new StringContent(JsonConvert.SerializeObject(mailItem), Encoding.UTF8, "application/json");
            response = await _client.PatchAsync(request+ key, httpRequestContent);
            response.EnsureSuccessStatusCode();

            // Check Unead Mail, should be 0
            response = await _client.GetAsync(request + recipient1 + "/unread");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

            // Check Unead Mail, should be 0
            response = await _client.GetAsync(request + recipient2 + "/unread");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

             // Check Read Mail for user 1, should be 1
            response = await _client.GetAsync(request + recipient1 + "/read");
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            mailItems = JsonConvert.DeserializeObject<List<MailItem>>(responseString);
            Assert.NotNull(mailItems);
            Assert.True(mailItems.Count == 1);
            key = mailItems[0].Key;

            // Delete MailItem for user 1
            response = await _client.DeleteAsync(request + key);
            response.EnsureSuccessStatusCode();

             // Check Read Mail for user 2, should be 1
            response = await _client.GetAsync(request + recipient2 + "/read");
            responseString = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseString);
            mailItems = JsonConvert.DeserializeObject<List<MailItem>>(responseString);
            Assert.NotNull(mailItems);
            Assert.True(mailItems.Count == 1);     
            key = mailItems[0].Key;       

            // Delete MailItem for user 2
            response = await _client.DeleteAsync(request + key);
            response.EnsureSuccessStatusCode();

            // Check Mail, should be 0
            response = await _client.GetAsync(request + recipient1 + "/all");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

            // Check Mail, should be 0
            response = await _client.GetAsync(request + recipient2 + "/all");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

            if(groupMembers.Count > 0) {
                foreach(GroupMember _groupMember in groupMembers) {
                    response = await _client.DeleteAsync(request + _groupMember.Key);
                }
            }
        }
    }
}