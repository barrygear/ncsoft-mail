/*
 * Project: NCSoft-Mail
 * File: src/Test/MailRepositoryTest.cs
 */
using System;
using Mail;
using Xunit;
using Xunit.Abstractions;

namespace MailTests
{
    public class MailRepositoryTest : IClassFixture<MailRepositoryFixture>
    {
            private readonly ITestOutputHelper output;
        public MailRepositoryTest(MailRepositoryFixture mailRepositoryFixture, ITestOutputHelper output)
        {
            mailRepository = mailRepositoryFixture.MailRespository;

            this.output = output;
        }

        public IMailRepository mailRepository { get; set; }

   /*
    * This test 
    * 1) checks for a random key value. It should be null.
    * 2) creates a mail, save it under key
    * 2) ensure the mail is readable using key
    *
    */
        [Fact]
        public void CreateMail()
        {
            string Key = Guid.NewGuid().ToString();

            MailItem mailItem = mailRepository.Find(Key);
            Assert.Null(mailItem);

            mailItem = new MailItem();
            mailItem.Key = Key;
            mailItem.Sender = "Sender";
            mailItem.Recipient = "Recipient";
            mailItem.Subject = "Subject";
            mailItem.Message = "Message";

            mailRepository.Add(mailItem);
           
            // Assert
            mailItem = mailRepository.Find(Key);
            Assert.NotNull(mailItem);
        }
    }
}