/*
 * Project: NCSoft-Mail
 * File: src/Test/MailUserRepositoryFixture.cs
 */
using System;
using Mail;
public class MailUserRepositoryFixture : IDisposable
{
    public MailUserRepositoryFixture()
    {
        // MailUserRepository = new MailUserRepository_NoDb();

        // ... initialize data in the test database ...
    }

    public void Dispose()
    {
        // ... clean up test data from the database ...
    }

    public IMailUserRepository MailUserRepository { get; private set; }
}