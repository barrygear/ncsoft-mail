/*
 * Project: NCSoft-Mail
 * File: src/Controllers/GroupController.cs
 */

/*
 * The GroupController allows for CRUD of GroupMember objects. It supports the following REST calls:
 *
 * GET /Group/              Returns All Group Entries
 * GET /Group/{key}         Returns the entry for the key (i.e. unique search by key)
 * GET /Group/{name}/name   Returns the entry/entryies for the Group name (i.e. search by Group name)
 * DELETE /Group/{key}      Delete entry by key
 * POST /Group              Create a Group entry
 *                          example payload : { "Name": "MyGroup", "Recipient": "test@gmail.com" }
 */
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Mail
{
    [Route("/api/[controller]")]
    public class GroupController : Controller
    {
        public GroupController(IGroupRepository groupMembers, IMailUserRepository mailUsers)
		{
			GroupMembersRepository = groupMembers;
            MailUserRepository = mailUsers;
		}
        public IGroupRepository GroupMembersRepository { get; set; }
		public IMailUserRepository MailUserRepository { get; set; }
		
        [HttpGet]
        public IEnumerable<GroupMember> GetAll()
        {
            return GroupMembersRepository.GetAll();
        }

        [HttpGet("{key}", Name = "GetGroupMemberByKey")]
        public IActionResult GetGroupMemberByKey(string key)
        {
            var groupMember = GroupMembersRepository.Find(key);

            if (groupMember == null)
            {
                return new NotFoundResult();
            }

            return new OkObjectResult(groupMember);
        }

        [HttpGet("{name}/name", Name = "GetGroupMembersByName")]
        public IActionResult GetGroupMembersByName(string name)
        {
            var groupMember = GroupMembersRepository.FindAllByName(name);
            if (groupMember == null)
            {
                return new NotFoundResult();
            }
            return new OkObjectResult(groupMember);
        }

        [HttpDelete("{key}", Name = "DeleteGroupMemberByKey")]
        public IActionResult DeleteGroupMemberByKey(string key)
        {
            var groupMember = GroupMembersRepository.Remove(key);
            if (groupMember == null)
            {
                return new NotFoundResult();
            }
            return new OkObjectResult(groupMember);
        }

        [HttpPost(Name = "Create")]
        public IActionResult Create([FromBody]GroupMember groupMember)
        {
            if (groupMember == null)
			{
				return BadRequest();
			}
            MailUser mailUser = MailUserRepository.FindByRecipient(groupMember.Recipient);
            if(mailUser == null) {
                return BadRequest("Recipient does not exist");
            }
            GroupMember existingMember = GroupMembersRepository.FindByNameAndRecipient(groupMember.Name, groupMember.Recipient);
			if(existingMember != null) {
                return BadRequest("Recipient already joined!");
            } else {
                GroupMembersRepository.Add(groupMember);
            }
			return CreatedAtRoute("GetGroupMemberByKey", new { key = groupMember.Key }, groupMember);
        }
    }
}