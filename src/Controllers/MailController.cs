/*
 * Project: NCSoft-Mail
 * File: src/Controllers/MailController.cs
 */

/*
* The MailController allows for CRUD of Mail objects to facillitate sending and recieving of mail. 
* It supports the following REST calls:
*
* GET /Mail/             			Returns all Mail entries
* GET /Mail/{key}         			Returns the entry for the key (i.e. unique search by key)
* GET /Mail/{sender}/sender 		Returns the entry/entryies sent by {sender} (i.e. search by sender)
* GET /Mail/{receiver}/sent		Returns the read entry/entryies sent to {receiver} (i.e. search by receiver, read)
* GET /Mail/{receiver}/unread		Returns the unread entry/entryies sent to {receiver} (i.e. search by receiver, unread)
* GET /Mail/{receiver}/all			Returns all entry/entryies sent to {receiver} (i.e. search by receiver)
* PUT /Mail/{key}      			Replace entry by key. Not very useful in practice but good for testing
*                         			example payload : {
*											"Key": "1f184ec8-8750-45aa-ab37-6bfb0a6590bc",
*	 										"Sender": "sender@gmail.com", 
*											"Recipient": "test@gmail.com", 
*											"Subject": "test mail",
*											"Message" : "test message body" }
* PATCH /Mail/{key}      			Delete entry by key
*                         			example payload : { "Read": false }
* DELETE /Mail/{key}      			Delete entry by key
* POST /Mail              			Create a Mail entry. Recipient can be a Recipient from MailUser or Name from a Group 
*                         			example payload : {
*	 										"Sender": "sender@gmail.com", 
*											"Recipient": "test@gmail.com", 
*											"Subject": "test mail",
*											"Message" : "test message body" }
*/
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Mail
{
    [Route("api/[controller]")]
    public class MailController : Controller
    {
        public MailController(IMailRepository mailItems, IMailUserRepository mailUsers, IGroupRepository groups)
        {
            MailItemsRepository = mailItems;
            MailUserRepository = mailUsers;
            GroupMemberRepository = groups;
        }
        public IMailRepository MailItemsRepository { get; set; }
        public IMailUserRepository MailUserRepository { get; set; }
        public IGroupRepository GroupMemberRepository { get; set; }

        #region snippet_GetAll
        [HttpGet]
        public IEnumerable<MailItem> GetAll()
        {
            return MailItemsRepository.GetAll();
        }
        #endregion

        #region snippet_GetMailByKey
        [HttpGet("{key}", Name = "GetMailByKey")]
        public IActionResult GetMailByKey(string key)
        {
            var item = MailItemsRepository.Find(key);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
        #endregion


        #region snippet_FindAllBySender
        [HttpGet("{sender}/sender", Name = "FindAllBySender")]
        public IActionResult FindAllBySender(string sender)
        {
            var item = MailItemsRepository.FindAllBySender(sender);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
        #endregion

        #region snippet_FindAllByRecipient

        [HttpGet("{recipient}/all", Name = "FindAllByRecipient")]
        public IActionResult FindAllByRecipient(string recipient)
        {
            return FindAllByRecipientInternal(recipient, null);
        }

        [HttpGet("{recipient}/read", Name = "FindAllReadByRecipient")]
        public IActionResult FindAllReadByRecipient(string recipient)
        {
            return FindAllByRecipientInternal(recipient, true);
        }

        [HttpGet("{recipient}/unread", Name = "FindAllUnreadByRecipient")]
        public IActionResult FindAllUnreadByRecipient(string recipient)
        {
            return FindAllByRecipientInternal(recipient, false);
        }

        public IActionResult FindAllByRecipientInternal(string recipient, Boolean? readStatus)
        {
            List<MailItem> result = new List<MailItem>();
            IEnumerable<GroupMember> groups = GroupMemberRepository.FindAllByRecipient(recipient);
			if (readStatus == null)
            {
                IEnumerable<MailItem> _mail = MailItemsRepository.FindAllByRecipient(recipient);
                if (_mail != null)
                {
                    result.AddRange(_mail);
                }
                if(groups != null) { 
                    foreach (GroupMember groupMember in groups)
                    {
                        _mail = MailItemsRepository.FindAllByRecipient(groupMember.Name);
                        if (_mail != null)
                        {
                            result.AddRange(_mail);
                        }
                    }
                }
            }
            else
            {
                IEnumerable<MailItem> _mail = MailItemsRepository.FindAllByRecipientWithReadStatus(recipient, (bool)readStatus);
				if (_mail != null)
				{
					result.AddRange(_mail);
				}
                if(groups != null) {
                    foreach (GroupMember groupMember in groups)
                    {
                        _mail = MailItemsRepository.FindAllByRecipientWithReadStatus(groupMember.Name, (bool)readStatus);
                        if (_mail != null)
                        {
                            result.AddRange(_mail);
                        }
                    }
                }
            }
            if (result == null || result.Count == 0)
            {
                return NotFound();
            }
            return new ObjectResult(result);
        }
        #endregion

        #region snippet_Create
        [HttpPost(Name = "CreateMail")]
        public IActionResult CreateMail([FromBody] MailItem mail)
        {
            if (mail == null)
            {
                return BadRequest("No Data!");
            }
            else if (mail.Sender == null)
            {
                return BadRequest("Missing Sender");
            }
            else if (mail.Recipient == null)
            {
                return BadRequest("Missing Recipient");
            }
            else if (mail.Subject == null)
            {
                return BadRequest("Missing Subject");
            }
            else if (mail.Message == null)
            {
                return BadRequest("Missing Message");
            }
            MailUser mailUser = MailUserRepository.FindByRecipient(mail.Recipient);
			bool isGroup = GroupMemberRepository.Exists(mail.Recipient);
            if (mailUser == null && !isGroup)
            {
                return BadRequest("Unknown Recipient");
            }
			if(isGroup) {
				IEnumerable<GroupMember> members = GroupMemberRepository.FindAllByName(mail.Recipient);
                foreach(GroupMember member in members) {
                    MailItem _mailItem = new MailItem { 
                        Sender = member.Name, 
                        Recipient = member.Recipient,
                        Read = mail.Read,
                        Subject = mail.Subject,
                        Message = mail.Message,
                        };
					MailItemsRepository.Add(_mailItem);		
                    mail = _mailItem;		
				}                
			} else {
        	    MailItemsRepository.Add(mail);
			}
            return CreatedAtRoute("GetMailByKey", new { key = mail.Key }, mail);
        }
        #endregion

        #region snippet_Replace
        [HttpPut("{key}", Name = "Replace")]
        public IActionResult Replace(string key, [FromBody] MailItem mail)
        {
            if (mail == null || mail.Key != key)
            {
                return BadRequest();
            }

            var _mail = MailItemsRepository.Find(key);
            if (_mail == null)
            {
                return NotFound();
            }

            MailItemsRepository.Update(mail);
            return new NoContentResult();
        }
        #endregion

        #region snippet_Patch
        [HttpPatch("{key}", Name = "Update")]
        public IActionResult Update(string key, [FromBody] MailItem mail)
        {
            if (mail == null)
            {
                return BadRequest();
            }

            var _mail = MailItemsRepository.Find(key);
            if (_mail == null)
            {
                return NotFound();
            }

            // copy the key since the entry from the payload may not have it
            mail.Key = _mail.Key;

            if(mail.Sender == null) {
                mail.Sender = _mail.Sender;            
            }
            if(mail.Recipient == null) {
                mail.Recipient = _mail.Recipient;            
            }
            if(mail.Subject == null) {
                mail.Subject = _mail.Subject;            
            }
            if(mail.Message == null) {
                mail.Message = _mail.Message;            
            }
            if(mail.Read == null) {
                mail.Read = _mail.Read;            
            }
            MailItemsRepository.Update(mail);
            return new NoContentResult();
        }
        #endregion

        #region snippet_Delete
        [HttpDelete("{key}", Name = "Delete")]
        public IActionResult Delete(string key)
        {
            var todo = MailItemsRepository.Find(key);
            if (todo == null)
            {
                return NotFound();
            }

            MailItemsRepository.Remove(key);
            return new NoContentResult();
        }
        #endregion
    }
}