/*
 * Project: NCSoft-Mail
 * File: src/Controllers/MailUser.cs
 */
 /*
 * The MailUser controller allows for the creation of Mail Users. This must be done before sending mail.
 *
 * GET /MailUser/                       Returns all MailUser entries
 * GET /MailUser/{name}/name            Returns the entry for the MailUser name (i.e. unique search by name)
 * GET /MailUser/{recipient}/recipient  Returns the entry for the MailUser recipient (i.e. unique search by recpient)
 * DELETE /MailUser/{key}               Delete entry by key
 * POST /MailUser                       Create a Group entry
 *                                      example payload : { "Name": "TestUser", "Recipient": "test@gmail.com" }
 */
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace Mail
{
    [Route("/api/[controller]")]
    public class MailUserController : Controller
    {
        public MailUserController(IMailUserRepository mailUsers)
		{
			MailUserRespository = mailUsers;
		}
        public IMailUserRepository MailUserRespository { get; set; }

#region snippet_GetAll
        [HttpGet]
        public IEnumerable<MailUser> GetAll()
        {
            return MailUserRespository.GetAll();
        }
#endregion

#region snippet_GetMailUserByName
        [HttpGet("{name}/name", Name = "GetMailUserByName")]
        public IActionResult GetMailUserByName(string name)
        {
            var mailUser = MailUserRespository.FindByName(name);
            if (mailUser == null)
            {
                return new NotFoundResult();
            }
            return new OkObjectResult(mailUser);
        }
#endregion

#region snippet_GetMailUserByRecipient
        [HttpGet("{recipient}/recipient", Name = "GetMailUserByRecipient")]
        public IActionResult GetMailUserByRecipient(string recipient)
        {
            var mailUser = MailUserRespository.FindByRecipient(recipient);
            if (mailUser == null)
            {
                return new NotFoundResult();
            }
            return new OkObjectResult(mailUser);
        }
#endregion

#region snippet_DeleteMailUserByKey
        [HttpDelete("{key}", Name = "DeleteMailUserByKey")]
        public IActionResult DeleteMailUserByKey(string key)
        {
            var mailUser = MailUserRespository.Remove(key);
            if (mailUser == null)
            {
                return new NotFoundResult();
            }
            return new OkObjectResult(mailUser);
        }
#endregion

#region snippet_Post
        [HttpPost(Name = "CreateUser")]
        public IActionResult Post([FromBody]MailUser mailUser)
        {
            if (mailUser == null)
			{
				return BadRequest();
			} else if (mailUser.Name == null) {
                return BadRequest("Missing Name");
            } else if (mailUser.Recipient == null) {
                return BadRequest("Missing Recipient");
            }
            MailUser existingUser = MailUserRespository.FindByName(mailUser.Name);
            if(existingUser == null) {
                MailUserRespository.Add(mailUser);
            }
			return CreatedAtRoute("GetMailUserByName", new { name = mailUser.Name }, mailUser);
        }
    }
#endregion
}