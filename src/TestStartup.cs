/*
 * Project: NCSoft-Mail
 * File: src/TestStartup.cs
 */
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NoDb;
// using Swashbuckle.Swagger.Model;
// using Basic.Swagger;

namespace Mail
{
    public class TestStartup
    {
        // private string GetSwaggerXMLPath()
	    // {
	        // var app = PlatformServices.Default.Application;
	        // return System.IO.Path.Combine(app.ApplicationBasePath, "NCSoft-Mail.xml");
	    // }

        static public string DefaultConnectionString { get; } =
        @"server=localhost;uid=root;persistsecurityinfo=False;port=3306;SslMode=None;database=ncsoft";
        static IReadOnlyDictionary<string, string> DefaultConfigurationStrings { get; } =
            new Dictionary<string, string>()
            {
                ["Profile:UserName"] = Environment.GetEnvironmentVariable("USER"),
                [$"AppConfiguration:DataStore"] = "Mem",
                //[$"AppConfiguration:DataStore"] = "NoDB",
                [$"AppConfiguration:ConnectionString"] = DefaultConnectionString
            };
        static public IConfiguration Configuration { get; set; }
        public TestStartup(IHostingEnvironment env)
		{
            
			var builder = new ConfigurationBuilder()
			    .AddInMemoryCollection(DefaultConfigurationStrings)
            	.SetBasePath(env.ContentRootPath)
				// .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
				// .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
				.AddEnvironmentVariables();
			Configuration = builder.Build();

		}

        #region snippet_AddSingleton
        public void ConfigureServices(IServiceCollection services)
        { 
            services.AddMvcCore()
                    .AddJsonFormatters();
		            // .AddSwaggerGen();
            // services.ConfigureSwaggerGen(options =>
            // {
            //     options.SingleApiVersion(new Info
            //     {
            //         Version = "v1",
            //         Title = "Test API",
            //         Description = "Jesse's Test API",
            //         TermsOfService = "None",
            //         Contact = new Contact() { Name = "JESSE.NET", Email = "info@test.com", Url = "www.JesseDotNet.com" }
            //     });
            //     options.IncludeXmlComments(GetSwaggerXMLPath());
            //     options.DescribeAllEnumsAsStrings();
            // });

            if($"{Configuration["AppConfiguration:DataStore"]}" == "Mysql") {
                services.AddSingleton<IMailRepository, MailRepository_Mysql>();
                services.AddSingleton<IMailUserRepository, MailUserRepository_Mysql>();
                services.AddSingleton<IGroupRepository, GroupRepository_Mysql>();
            } else if($"{Configuration["AppConfiguration:DataStore"]}" == "NoDB") {
                // if you want to plugin a custom serializer or pathresolver for your type,
                // register those before calling .AddNoDb<YourType>();
                //services.AddScoped<NoDb.IStringSerializer<YourType>, YourTypeSerializer>();
                //services.AddScoped<NoDb.IStoragePathResolver<YourType>, YourTypeStoragePathResolver>();

                services.AddNoDb<MailUser>();
                services.AddNoDb<MailItem>();
                services.AddNoDb<GroupMember>();
                services.AddSingleton<IMailRepository, MailRepository_NoDb>();
                services.AddSingleton<IGroupRepository, GroupRepository_NoDb>();
                services.AddSingleton<IMailUserRepository, MailUserRepository_NoDb>();
            } else {
                services.AddSingleton<IMailRepository, MailRepository_Mem>();
                services.AddSingleton<IMailUserRepository, MailUserRepository_Mem>();
                services.AddSingleton<IGroupRepository, GroupRepository_Mem>();
            }
            
        }
        #endregion
        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(LogLevel.Debug);

            app.UseMvc();
	    // app.UseSwagger();
	    // app.UseSwaggerUi();
        }
    }
}
