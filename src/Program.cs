/*
 * Project: NCSoft-Mail
 * File: src/Program.cs
 */
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;

namespace Mail
{
    public class Program
    {
        public static void Main(string[] args = null)
        {
            var host = new WebHostBuilder()
                        .UseKestrel()
                        .UseContentRoot(Directory.GetCurrentDirectory())
                        .UseIISIntegration()
                        .UseStartup<Startup>()
                        .Build();
            host.Run();
        }
    }
}