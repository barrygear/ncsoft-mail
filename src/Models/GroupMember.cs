/*
 * Project: NCSoft-Mail
 * File: src/Models/GroupMember.cs
 */

/*
 * This is a GroupMember object. Its used to make mailing groups. You can insert one with a group name and a Recipient
 * to create the group association.
 */

namespace Mail
{
    public class GroupMember
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Recipient { get; set; }

        public GroupMember() {}

        public GroupMember(string Name, string Recipient) {
            this.Name = Name;
            this.Recipient = Recipient;
        }
    }
}