/*
 * Project: NCSoft-Mail
 * File: src/Models/MailUser.cs
 */

/*
 * This is a MailUser object. Its a simple user object that contains a username and a recient address. A MailUser must
 * be created before sending mail. 
 */
namespace Mail
{
    public class MailUser
    {
        public MailUser() {}

        public MailUser(string name, string recipient) {
            this.Name = name;
            this.Recipient = recipient;
        }
        
        public int Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Recipient { get; set; }
    }
}