/*
 * Project: NCSoft-Mail
 * File: src/Models/MailItem.cs
 */
 
/*
 * This is a MailItem object. It represents mail sent to a destination. The Id field is used by
 * db backends and is ignored in NoDB backend. If reading for a specific mail, you search by Key. 
 *
 */ 
using System;

namespace Mail
{
    public class MailItem
    {
        public int Id { get; set; }
        public string Sender { get; set; }
        public string Key { get; set; }
		public string Subject { get; set; }
        public string Message { get; set; }
        public string Recipient { get; set; }
        // public Boolean? Deleted { get; set; }
        public Boolean? Read { get; set; }
        
        public MailItem() {
            Sender = null;
            Key = null;
            Read = false;
            Subject = null;
            Message = null;
            Recipient = null;
            // Deleted = false;
        }

        public MailItem(string sender, string recipient, string subject, String message) {
            Sender = sender;
            Key = null;
            Read = false;
            Subject = subject;
            Message = message;
            Recipient = recipient;
            // Deleted = false;
        }
    }
}