/*
 * Project: NCSoft-Mail
 * File: src/Repository/MailRepository_NoDb.cs
 */
using System;
using System.Collections.Generic;
using NoDb;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Linq;

namespace Mail
{
    public class MailRepository_NoDb : IMailRepository
    {
        private IBasicCommands<MailItem> commands;
        private IBasicQueries<MailItem> query;
        private ILogger<MailRepository_NoDb> log;

        public MailRepository_NoDb(IBasicCommands<MailItem> pageCommands,
                                    IBasicQueries<MailItem> pageQueries,
                                    ILogger<MailRepository_NoDb> logger)
        {
            commands = pageCommands;
            query = pageQueries;
            log = logger;

            // IEnumerable<MailItem> items = GetAll();
            // if (items.Count() == 0)
            // {
            //     Add(new MailItem { Sender = "Sender1" });
            // }
        }

        public IEnumerable<MailItem> GetAll()
        {
            Task<IEnumerable<MailItem>> task = query.GetAllAsync("mail");
            task.Wait();
            return task.Result;
        }

        public IEnumerable<MailItem> FindAllBySender(string sender)
        {
            List<MailItem> results = new List<MailItem>();
            IEnumerable<MailItem> _mail = GetAll();
            IEnumerable<MailItem> query = from _item in _mail
                                          where _item.Sender == sender
                                          orderby _item.Id
                                          select _item;

            foreach (MailItem item in query)
            {
                results.Add(item);
            }
            return results;
        }
        public IEnumerable<MailItem> FindAllByRecipient(string recipient)
        {
            List<MailItem> results = new List<MailItem>();
            IEnumerable<MailItem> _mail = GetAll();
            IEnumerable<MailItem> query = from _item in _mail
                                          where _item.Recipient == recipient
                                          orderby _item.Id
                                          select _item;

            foreach (MailItem item in query)
            {
                results.Add(item);
            }
            return results;
        }
        public IEnumerable<MailItem> FindAllByRecipientWithReadStatus(string recipient, bool readStatus)
        {
            List<MailItem> results = new List<MailItem>();
            IEnumerable<MailItem> _mail = GetAll();
            IEnumerable<MailItem> query = from _item in _mail
                                          where _item.Recipient == recipient && _item.Read == readStatus
                                          orderby _item.Id
                                          select _item;

            foreach (MailItem item in query)
            {
                results.Add(item);
            }
            return results;
        }
        public void Add(MailItem item)
        {
            if(item.Key == null) {
                item.Key = Guid.NewGuid().ToString();
            }
            commands.CreateAsync("mail", item.Key, item);
        }

        public MailItem Find(string key)
        {
            Task<MailItem> task = query.FetchAsync("mail", key);
            task.Wait();
            return task.Result;
        }

        public MailItem Remove(string key)
        {
            MailItem item = Find(key);
            commands.DeleteAsync("mail", key);
            return item;
        }

        public void Update(MailItem item)
        {
            MailItem originalItem = Find(item.Key);
            if(item.Message != null) {
                originalItem.Message = item.Message;
            }
            if(item.Read != null) {
                originalItem.Read = item.Read;
            }
            commands.UpdateAsync("mail", item.Key, originalItem);
        }
    }
}