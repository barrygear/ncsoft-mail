/*
 * Project: NCSoft-Mail
 * File: src/Repository/MailUserRepository_Mem.cs
 */
using System.Collections.Generic;
using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;
using System.Linq;
using System;

namespace Mail
{
    public class MailUserRepository_Mem : IMailUserRepository
    {
        private ILogger<MailUserRepository_Mem> log;

        private static ConcurrentDictionary<string, MailUser> _mailUsers = new ConcurrentDictionary<string, MailUser>();

        public MailUserRepository_Mem(ILogger<MailUserRepository_Mem> logger)
        {
            log = logger;
        }

        public IEnumerable<MailUser> GetAll()
        {
            return _mailUsers.Values;
        }

        public MailUser FindByRecipient(string recipient)
        {
            List<MailUser> results = new List<MailUser>();
            IEnumerable<MailUser> _mail = GetAll();
            IEnumerable<MailUser> query = from _item in _mail
                                          where _item.Recipient == recipient
                                          select _item;
            if (query.Count() > 0)
            {
                return query.ElementAt<MailUser>(0);
            }
            else
            {
                return null;
            }
        }
        public MailUser FindByName(string name)
        {
            List<MailUser> results = new List<MailUser>();
            IEnumerable<MailUser> _mail = GetAll();
            IEnumerable<MailUser> query = from _item in _mail
                                          where _item.Name == name
                                          select _item;
            if (query.Count() > 0)
            {
                return query.ElementAt<MailUser>(0);
            }
            else
            {
                return null;
            };
        }

        public void Add(MailUser item)
        {
            item.Key = Guid.NewGuid().ToString();
            _mailUsers[item.Key] = item;   
        }

        public MailUser Find(string key)
        {
            MailUser user;
            _mailUsers.TryGetValue(key, out user);
            return user;
        }

        public MailUser Remove(string key)
        {
            MailUser user;
            _mailUsers.TryRemove(key, out user);
            return user;
        }

        public void Update(MailUser item)
        {
            _mailUsers[item.Key] = item;
        }
    }
}