/*
 * Project: NCSoft-Mail
 * File: src/Repository/GroupRepository_Mem.cs
 */
using System.Collections.Generic;
using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;
using System.Linq;
using System;

namespace Mail
{
    public class GroupRepository_Mem : IGroupRepository
    {
        private ILogger<GroupRepository_Mem> log;
        private static ConcurrentDictionary<string, GroupMember> _groupMembers = new ConcurrentDictionary<string, GroupMember>();

        public GroupRepository_Mem(ILogger<GroupRepository_Mem> logger)
        {
            log = logger;
        }

        public IEnumerable<GroupMember> GetAll()
        {
            return _groupMembers.Values;
        }

        public IEnumerable<GroupMember> FindAllByRecipient(string recipient)
        {
            List<GroupMember> results = new List<GroupMember>();
            IEnumerable<GroupMember> _groupMembers = GetAll();
            IEnumerable<GroupMember> query = from _item in _groupMembers
                                             where _item.Recipient == recipient
                                             orderby _item.Id
                                             select _item;

            foreach (GroupMember item in query)
            {
                results.Add(item);
            }
            return results;
        }
        public IEnumerable<GroupMember> FindAllByName(string name)
        {
            List<GroupMember> results = new List<GroupMember>();
            IEnumerable<GroupMember> _groupMembers = GetAll();
            IEnumerable<GroupMember> query = from _item in _groupMembers
                                             where _item.Name == name
                                             orderby _item.Id
                                             select _item;

            foreach (GroupMember item in query)
            {
                results.Add(item);
            }
            return results;
        }
        public GroupMember FindByNameAndRecipient(string name, string recipient)
        {
            List<GroupMember> results = new List<GroupMember>();
            IEnumerable<GroupMember> _groupMembers = GetAll();
            IEnumerable<GroupMember> query = from _item in _groupMembers
                                             where _item.Name == name && _item.Recipient == recipient
                                             orderby _item.Id
                                             select _item;

            if(query.Count() > 0)
            {
                return query.ElementAt<GroupMember>(0);
            } else {
                return null;
            }
        }
        public void Add(GroupMember item)
        {
            if(item.Key == null) {
                item.Key = Guid.NewGuid().ToString();
            }
            _groupMembers[item.Key] = item;
        }

        public GroupMember Find(string key) {
            GroupMember groupMember;
            _groupMembers.TryGetValue(key, out groupMember);
            return groupMember;
        }

        public bool Exists(string name) {
            List<GroupMember> results = new List<GroupMember>();
            IEnumerable<GroupMember> _groupMembers = GetAll();
            IEnumerable<GroupMember> query = from _item in _groupMembers
                                             where _item.Name == name
                                             orderby _item.Id
                                             select _item;

            if(query.Count() > 0)
            {
                return true;
            } else {
                return false;
            }
        }

        public GroupMember Remove(string key)
        {
            GroupMember groupMember = Find(key);
            _groupMembers.TryRemove(key, out groupMember);
            return groupMember;
        }

        public void Update(GroupMember groupMember)
        {
            _groupMembers[groupMember.Key] = groupMember;
        }
    }
}