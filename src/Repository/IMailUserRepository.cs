/*
 * Project: NCSoft-Mail
 * File: src/Repository/IMailUserRepository.cs
 */
using System.Collections.Generic;

namespace Mail
{
	public interface IMailUserRepository
	{
		void Add(MailUser item);
		IEnumerable<MailUser> GetAll();
		MailUser FindByRecipient(string recipient);		
		MailUser FindByName(string name);		
		MailUser Find(string key);
		MailUser Remove(string key);
		void Update(MailUser item);
	}
}
