/*
 * Project: NCSoft-Mail
 * File: src/Repository/IMailRepository.cs
 */
using System.Collections.Generic;

namespace Mail
{
	public interface IMailRepository
	{
		void Add(MailItem item);
		IEnumerable<MailItem> GetAll();
		IEnumerable<MailItem> FindAllBySender(string sender);
		IEnumerable<MailItem> FindAllByRecipient(string recipient);	
		IEnumerable<MailItem> FindAllByRecipientWithReadStatus(string recipient, bool readStatus);		
		MailItem Find(string key);
		MailItem Remove(string key);
		void Update(MailItem item);
	}
}
