/*
 * Project: NCSoft-Mail
 * File: src/Repository/IGroupRepository.cs
 */
using System.Collections.Generic;

namespace Mail
{
	public interface IGroupRepository
	{
		void Add(GroupMember item);
		IEnumerable<GroupMember> GetAll();
		IEnumerable<GroupMember> FindAllByRecipient(string recipient);		
		IEnumerable<GroupMember> FindAllByName(string name);	
		GroupMember FindByNameAndRecipient(string name, string recipient);	
		bool Exists(string name);	
		GroupMember Find(string key);
		GroupMember Remove(string key);
		void Update(GroupMember item);
	}
}
