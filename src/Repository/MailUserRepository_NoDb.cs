/*
 * Project: NCSoft-Mail
 * File: src/Repository/MailUserRepository_NoDb.cs
 */
using System.Collections.Generic;
using NoDb;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace Mail
{
    public class MailUserRepository_NoDb : IMailUserRepository
    {
        private IBasicCommands<MailUser> commands;
        private IBasicQueries<MailUser> query;
        private ILogger<MailUserRepository_NoDb> log;

        public MailUserRepository_NoDb(IBasicCommands<MailUser> pageCommands,
                                    IBasicQueries<MailUser> pageQueries,
                                    ILogger<MailUserRepository_NoDb> logger)
        {
            commands = pageCommands;
            query = pageQueries;
            log = logger;

            // IEnumerable<MailUser> items = GetAll();
            // foreach (MailUser item in items)
            // {
            //     if(item.Key == null) {
            //         Remove("0");
            //     } else {
            //         Remove(item.Key);
            //     }
            // }
            // Add(new MailUser { Recipient = "barrygear@gmail.com", Name = "Barry Gear" });
        }

        public IEnumerable<MailUser> GetAll()
        {
            Task<IEnumerable<MailUser>> task = query.GetAllAsync("MailUser");
            task.Wait();
            return task.Result;
        }

        public MailUser FindByRecipient(string recipient)
        {
            List<MailUser> results = new List<MailUser>();
            IEnumerable<MailUser> _mail = GetAll();
            IEnumerable<MailUser> query = from _item in _mail
                                          where _item.Recipient == recipient
                                          select _item;
            if (query.Count() > 0)
            {
                return query.ElementAt<MailUser>(0);
            }
            else
            {
                return null;
            }
        }
        public MailUser FindByName(string name)
        {
            List<MailUser> results = new List<MailUser>();
            IEnumerable<MailUser> _mail = GetAll();
            IEnumerable<MailUser> query = from _item in _mail
                                          where _item.Name == name
                                          select _item;
            if (query.Count() > 0)
            {
                return query.ElementAt<MailUser>(0);
            }
            else
            {
                return null;
            };
        }

        public void Add(MailUser item)
        {
            item.Key = Guid.NewGuid().ToString();
            commands.CreateAsync("MailUser", item.Key, item);
        }

        public MailUser Find(string key)
        {
            Task<MailUser> task = query.FetchAsync("MailUser", key);
            task.Wait();
            return task.Result;
        }

        public MailUser Remove(string key)
        {
            MailUser item = Find(key);
            commands.DeleteAsync("MailUser", key);
            return item;
        }

        public void Update(MailUser item)
        {
            commands.UpdateAsync("MailUser", item.Key, item);
        }
    }
}