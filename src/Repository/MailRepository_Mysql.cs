/*
 * Project: NCSoft-Mail
 * File: src/Repository/MailRepository_Mysql.cs
 */
using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace Mail
{
    public class MailRepository_Mysql : IMailRepository
    {
        MySqlConnection connection = null;
        public MailRepository_Mysql()
        {
            connection = new MySqlConnection
            {
                // ConnectionString = "server=localhost;uid=root;persistsecurityinfo=False;port=3306;SslMode=None;database=ncsoft"
                ConnectionString = ($"{Startup.Configuration["AppConfiguration:ConnectionString"]}")
            };
        }

        public IEnumerable<MailItem> GetAll()
        {
            List<MailItem> _mail = new List<MailItem>();

            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("SELECT mail.id, mail.key, mail.sender, " + 
                "mail.read, mail.recipient, mail.message, mail.subject FROM mail;", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    if(reader.HasRows) {
                        while (reader.Read())
                        {
                            int _id = reader.GetInt16(0);
                            string _key = reader.GetString(1);
                            string _sender = reader.GetString(2);
                            bool _read = reader.GetBoolean(3);
                            string _recipient = reader.GetString(4);
                            string _message = reader.GetString(5);
                            string _subject = reader.GetString(6);

                            _mail.Add(new MailItem { 
                                Sender = _sender, 
                                Id = _id, 
                                Key = _key, 
                                Read = _read, 
                                Recipient = _recipient,
                                Message = _message,
                                Subject = _subject });
                        }
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return _mail;
        }

        public IEnumerable<MailItem> FindAllBySender(string sender)
        {
            List<MailItem> _mail = new List<MailItem>();

            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("SELECT mail.id, mail.key, mail.sender, " + 
                "mail.read, mail.recipient, mail.message, mail.subject FROM mail where mail.sender=\"" + sender + 
                "\" order by mail.id;", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int _id = reader.GetInt16(0);
                        string _key = reader.GetString(1);
                        string _sender = reader.GetString(2);
                        bool _read = reader.GetBoolean(3);
                        string _recipient = reader.GetString(4);
                        string _message = reader.GetString(5);
                        string _subject = reader.GetString(6);
                
                        _mail.Add(new MailItem { 
                            Sender = _sender, 
                            Id = _id, 
                            Key = _key, 
                            Read = _read, 
                            Recipient = _recipient,
                            Message = _message,
                            Subject = _subject });
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return _mail;
        }

        public IEnumerable<MailItem> FindAllByRecipient(string recipient)
        {
            List<MailItem> _mail = new List<MailItem>();

            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("SELECT mail.id, mail.key, mail.sender, " + 
                "mail.read, mail.recipient, mail.message, mail.subject FROM mail where mail.recipient=\"" + recipient + 
                "\" order by mail.id;", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int _id = reader.GetInt16(0);
                        string _key = reader.GetString(1);
                        string _sender = reader.GetString(2);
                        bool _read = reader.GetBoolean(3);
                        string _recipient = reader.GetString(4);
                        string _message = reader.GetString(5);
                        string _subject = reader.GetString(6);
                     
                        _mail.Add(new MailItem { 
                            Sender = _sender, 
                            Id = _id, 
                            Key = _key, 
                            Read = _read, 
                            Recipient = _recipient,
                            Message = _message,
                            Subject = _subject });
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return _mail;
        }

        public IEnumerable<MailItem> FindAllByRecipientWithReadStatus(string recipient, bool readStatus)
        {
            List<MailItem> _mail = new List<MailItem>();

            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand(
                    "SELECT mail.id, mail.key, mail.sender, mail.read, mail.recipient, mail.message, mail.subject " + 
                    "FROM mail where mail.recipient=\"" + recipient + "\" and mail.read =" + 
                    readStatus + " order by mail.id;", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int _id = reader.GetInt16(0);
                        string _key = reader.GetString(1);
                        string _sender = reader.GetString(2);
                        bool _read = reader.GetBoolean(3);
                        string _recipient = reader.GetString(4);
                        string _message = reader.GetString(5);
                        string _subject = reader.GetString(6);

                        _mail.Add(new MailItem { 
                            Sender = _sender, 
                            Id = _id, 
                            Key = _key, 
                            Read = _read, 
                            Recipient = _recipient,
                            Message = _message,
                            Subject = _subject });
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return _mail;
        }

        public void Add(MailItem item)
        {
            item.Key = Guid.NewGuid().ToString();

            connection.Open();
            try
            {
                MySqlCommand command = new MySqlCommand("INSERT into mail (mail.key, mail.sender, mail.read, " + 
                "mail.recipient, mail.message, mail.subject) values (\""
                    + item.Key
                    + "\", \""
                    + item.Sender
                    + "\", "
                    + item.Read
                    + ", \""
                    + item.Recipient
                    + "\", \""
                    + item.Message
                    + "\", \""
                    + item.Subject
                    + "\");"
                    , connection);
                int result = command.ExecuteNonQuery();
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }

        public MailItem Find(string key)
        {
            MailItem _mail = null;

            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("SELECT mail.id, mail.key, mail.sender, mail.read, " + 
                "mail.recipient, mail.message, mail.subject FROM mail where mail.key=\"" + key + "\" limit 1;", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    if(reader.HasRows) {
                        reader.Read();

                        int _id = reader.GetInt16(0);
                        string _key = reader.GetString(1);
                        string _sender = reader.GetString(2);
                        bool _read = reader.GetBoolean(3);
                        string _recipient = reader.GetString(4);
                        string _message = reader.GetString(5);
                        string _subject = reader.GetString(6);

                        _mail = (new MailItem { 
                            Sender = _sender, 
                            Id = _id, 
                            Key = _key, 
                            Read = _read, 
                            Recipient = _recipient,
                            Message = _message,
                            Subject = _subject });
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return _mail;
        }

        public MailItem Remove(string key)
        {
            MailItem _mail = null;
            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("SELECT mail.id, mail.key, mail.sender, " + 
                "mail.read, mail.recipient, mail.message, mail.subject FROM mail where mail.key=\"" + key + "\" limit 1;", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {                    
                    if (reader.HasRows)
                    {
                        reader.Read();

                        int _id = reader.GetInt16(0);
                        string _key = reader.GetString(1);
                        string _sender = reader.GetString(2);
                        bool _read = reader.GetBoolean(3);
                        string _recipient = reader.GetString(4);
                        string _message = reader.GetString(5);
                        string _subject = reader.GetString(6);

                        _mail = (new MailItem { 
                            Sender = _sender, 
                            Id = _id, 
                            Key = _key, 
                            Read = _read, 
                            Recipient = _recipient,
                            Message = _message,
                            Subject = _subject });

                        reader.Close();

                        command = new MySqlCommand("DELETE FROM mail where mail.key=\"" + key + "\" limit 1;", connection);
                        command.ExecuteNonQuery();
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return _mail;
        }

        public void Update(MailItem item)
        {
            try
            {
                connection.Open();
                bool valueAdded = false;
                string updateCommand = "UPDATE mail SET ";
                
                if (item.Sender != null)
                {
                    if (valueAdded)
                    {
                        updateCommand += ", ";
                    }
                    updateCommand += "mail.sender=\"" + item.Sender + "\"";
                    valueAdded = true;
                }

                if (item.Recipient != null)
                {
                    if (valueAdded)
                    {
                        updateCommand += ", ";
                    }
                    updateCommand += "mail.recipient=\"" + item.Recipient + "\"";
                    valueAdded = true;
                }

                if (item.Message != null)
                {
                    if (valueAdded)
                    {
                        updateCommand += ", ";
                    }
                    updateCommand += "mail.message=\"" + item.Message + "\"";
                    valueAdded = true;
                }

                if (item.Subject != null)
                {
                    if (valueAdded)
                    {
                        updateCommand += ", ";
                    }
                    updateCommand += "mail.subject=\"" + item.Subject + "\"";
                    valueAdded = true;
                }

                if (item.Read != null)
                {
                    if (valueAdded)
                    {
                        updateCommand += ", ";
                    }
                    updateCommand += "mail.read=" + item.Read;
                    valueAdded = true;
                }

                MySqlCommand command = new MySqlCommand(updateCommand + " where mail.key=\"" + item.Key + "\";", connection);

                command.ExecuteNonQuery();
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }
    }
}