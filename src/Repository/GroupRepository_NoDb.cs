/*
 * Project: NCSoft-Mail
 * File: src/Repository/GroupRepository_NoDb.cs
 */
using System.Collections.Generic;
using NoDb;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace Mail
{
    public class GroupRepository_NoDb : IGroupRepository
    {
        private IBasicCommands<GroupMember> commands;
        private IBasicQueries<GroupMember> query;
        private ILogger<GroupRepository_NoDb> log;

        public GroupRepository_NoDb(IBasicCommands<GroupMember> pageCommands,
                                    IBasicQueries<GroupMember> pageQueries,
                                    ILogger<GroupRepository_NoDb> logger)
        {
            commands = pageCommands;
            query = pageQueries;
            log = logger;

            // IEnumerable<GroupMember> items = GetAll();
            // foreach (GroupMember item in items)
            // {
            //     Remove("0");
            // }
            // Add(new GroupMember { Recipient = "Sender1", Name = "Group1" });
        }

        public IEnumerable<GroupMember> GetAll()
        {
            Task<IEnumerable<GroupMember>> task = query.GetAllAsync("group");
            task.Wait();
            return task.Result;
        }

        public IEnumerable<GroupMember> FindAllByRecipient(string recipient)
        {
            List<GroupMember> results = new List<GroupMember>();
            IEnumerable<GroupMember> _mail = GetAll();
            IEnumerable<GroupMember> query = from _item in _mail
                                             where _item.Recipient == recipient
                                             orderby _item.Id
                                             select _item;

            foreach (GroupMember item in query)
            {
                results.Add(item);
            }
            return results;
        }
        public IEnumerable<GroupMember> FindAllByName(string name)
        {
            List<GroupMember> results = new List<GroupMember>();
            IEnumerable<GroupMember> _mail = GetAll();
            IEnumerable<GroupMember> query = from _item in _mail
                                             where _item.Name == name
                                             orderby _item.Id
                                             select _item;

            foreach (GroupMember item in query)
            {
                results.Add(item);
            }
            return results;
        }
        public GroupMember FindByNameAndRecipient(string name, string recipient)
        {
            List<GroupMember> results = new List<GroupMember>();
            IEnumerable<GroupMember> _mail = GetAll();
            IEnumerable<GroupMember> query = from _item in _mail
                                             where _item.Name == name && _item.Recipient == recipient
                                             orderby _item.Id
                                             select _item;

            if(query.Count() > 0)
            {
                return query.ElementAt<GroupMember>(0);
            } else {
                return null;
            }
        }
        public void Add(GroupMember item)
        {
            item.Key = Guid.NewGuid().ToString();
            commands.CreateAsync("group", item.Key, item);
        }

        public GroupMember Find(string key) {
            Task<GroupMember> task = query.FetchAsync("group", key);
            task.Wait();
            return task.Result;
        }

        public bool Exists(string name) {
            List<GroupMember> results = new List<GroupMember>();
            IEnumerable<GroupMember> _mail = GetAll();
            IEnumerable<GroupMember> query = from _item in _mail
                                             where _item.Name == name
                                             orderby _item.Id
                                             select _item;

            if(query.Count() > 0)
            {
                return true;
            } else {
                return false;
            }
        }

        public GroupMember Remove(string key)
        {
            GroupMember item = Find(key);
            commands.DeleteAsync("group", key);
            return item;
        }

        public void Update(GroupMember item)
        {
            commands.UpdateAsync("group", item.Id.ToString(), item);
        }
    }
}