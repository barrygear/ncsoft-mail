/*
 * Project: NCSoft-Mail
 * File: src/Repository/MailRepository_Mem.cs
 */
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;

namespace Mail
{
    public class MailRepository_Mem : IMailRepository
    {
        private static ConcurrentDictionary<string, MailItem> _mail = new ConcurrentDictionary<string, MailItem>();

        public MailRepository_Mem()
        {
            //Add(new MailItem { Sender = "Item1" });
        }

        public IEnumerable<MailItem> GetAll()
        {
            return _mail.Values;
        }
        public IEnumerable<MailItem> FindAllBySender(string sender)
        {
            List<MailItem> results = new List<MailItem>();
            IEnumerable<MailItem> query = from _item in _mail.Values
                                          where _item.Sender == sender
                                          orderby _item.Id
                                          select _item;

            foreach (MailItem item in query)
            {
                results.Add(item);
            }
            return results;
        }
        public IEnumerable<MailItem> FindAllByRecipient(string recipient)
        {
            List<MailItem> results = new List<MailItem>();
            IEnumerable<MailItem> query = from _item in _mail.Values
                                          where _item.Recipient == recipient
                                          orderby _item.Id
                                          select _item;

            foreach (MailItem item in query)
            {
                results.Add(item);
            }
            return results;
        }

        public IEnumerable<MailItem> FindAllByRecipientWithReadStatus(string recipient, bool readStatus)
        {
            List<MailItem> results = new List<MailItem>();
            IEnumerable<MailItem> query = from _item in _mail.Values
                                          where _item.Recipient == recipient && _item.Read == readStatus
                                          orderby _item.Id
                                          select _item;

            foreach (MailItem item in query)
            {
                results.Add(item);
            }
            return results;
        }

        public void Add(MailItem item)
        {
            if(item.Key == null) {
                item.Key = Guid.NewGuid().ToString();
            }
            _mail[item.Key] = item;            
        }

        public MailItem Find(string key)
        {
            MailItem item;
            _mail.TryGetValue(key, out item);
            return item;
        }

        public MailItem Remove(string key)
        {
            MailItem item;
            _mail.TryRemove(key, out item);
            return item;
        }

        public void Update(MailItem item)
        {
            _mail[item.Key] = item;
        }
    }
}