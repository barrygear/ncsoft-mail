/*
 * Project: NCSoft-Mail
 * File: src/Repository/GroupRepository_Mysql.cs
 */
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using System.Linq;
using System;
using MySql.Data.MySqlClient;

namespace Mail
{
    public class GroupRepository_Mysql : IGroupRepository
    {
        private MySqlConnection connection;

        private ILogger<GroupRepository_Mysql> log;

        public GroupRepository_Mysql(ILogger<GroupRepository_Mysql> logger)
        {
            log = logger;

            connection = new MySqlConnection
            {
                // ConnectionString = "server=localhost;uid=root;persistsecurityinfo=False;port=3306;SslMode=None;database=ncsoft"
                ConnectionString = ($"{Startup.Configuration["AppConfiguration:ConnectionString"]}")
            };
        }

        public IEnumerable<GroupMember> GetAll()
        {
            List<GroupMember> _groupMembers = new List<GroupMember>();

            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("SELECT mailgroup.id, mailgroup.key, mailgroup.name, " + 
                "mailgroup.recipient FROM mailgroup;", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int _id = reader.GetInt16(0);
                        string _key = reader.GetString(1);
                        string _name = reader.GetString(2);
                        string _recipient = reader.GetString(4);

                        _groupMembers.Add(new GroupMember { 
                            Id = _id, 
                            Key = _key, 
                            Name = _name, 
                            Recipient = _recipient});
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return _groupMembers;
        }

        public IEnumerable<GroupMember> FindAllByRecipient(string recipient)
        {
           List<GroupMember> groupMembers = new List<GroupMember>();

            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("SELECT mailgroup.id, mailgroup.key, mailgroup.name," + 
                " mailgroup.recipient FROM mailgroup where mailgroup.recipient=\"" + recipient + 
                "\" order by mailgroup.id;", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int _id = reader.GetInt16(0);
                        string _key = reader.GetString(1);
                        string _name = reader.GetString(2);
                        string _recipient = reader.GetString(3);
                
                        groupMembers.Add(new GroupMember {               
                            Id = _id, 
                            Key = _key, 
                            Name = _name, 
                            Recipient = _recipient });
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return groupMembers;
        }
        public IEnumerable<GroupMember> FindAllByName(string name)
        {
            List<GroupMember> groupMembers = new List<GroupMember>();

            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("SELECT mailgroup.id, mailgroup.key, mailgroup.name," + 
                " mailgroup.recipient FROM mailgroup where mailgroup.name=\"" + name + 
                "\" order by mailgroup.id;", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int _id = reader.GetInt16(0);
                        string _key = reader.GetString(1);
                        string _name = reader.GetString(2);
                        string _recipient = reader.GetString(3);
                
                        groupMembers.Add(new GroupMember {               
                            Id = _id, 
                            Key = _key, 
                            Name = _name, 
                            Recipient = _recipient });
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return groupMembers;
        }
        public GroupMember FindByNameAndRecipient(string name, string recipient)
        {
            GroupMember groupMember = null;

            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("SELECT mailgroup.id, mailgroup.key, mailgroup.name," + 
                " mailgroup.recipient FROM mailgroup where mailgroup.name=\"" + name + 
                "\" AND mailgroup.recipient=\"" + recipient + "\";", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    if(reader.HasRows) {
                        int _id = reader.GetInt16(0);
                        string _key = reader.GetString(1);
                        string _name = reader.GetString(2);
                        string _recipient = reader.GetString(3);
                
                        groupMember = new GroupMember {               
                            Id = _id, 
                            Key = _key, 
                            Name = _name, 
                            Recipient = _recipient };
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return groupMember;
        }
        public void Add(GroupMember item)
        {
           if(item.Key == null) {
                item.Key = Guid.NewGuid().ToString();
            }
            connection.Open();
            try
            {
                MySqlCommand command = new MySqlCommand("INSERT into mailgroup (mailgroup.key, mailgroup.name, mailgroup.recipient) values (\""
                    + item.Key
                    + "\", \""
                    + item.Name
                    + "\", \""
                    + item.Recipient
                    + "\");"
                    , connection);
                int result = command.ExecuteNonQuery();
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }

        public GroupMember Find(string key) {
            GroupMember groupMember = null;

            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("SELECT mailgroup.id, mailgroup.key, mailgroup.name, " + 
                "mailgroup.recipient FROM mailgroup where mailgroup.key=\"" + key + "\" limit 1;", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    if(reader.HasRows) {
                        reader.Read();

                        int _id = reader.GetInt16(0);
                        string _key = reader.GetString(1);
                        string _name = reader.GetString(2);
                        string _recipient = reader.GetString(3);

                        groupMember = new GroupMember { 
                            Id = _id, 
                            Key = _key, 
                            Name = _name, 
                            Recipient = _recipient };
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return groupMember;
        }

        public bool Exists(string name) {
            IEnumerable<GroupMember> members = FindAllByName(name);
            return members != null && members.Count() > 0;
        }

        public GroupMember Remove(string key)
        {
            GroupMember _groupMember = Find(key);
            if(_groupMember != null) {
                try
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand("DELETE FROM mailgroup where mailgroup.key=\"" + key + "\" limit 1;", connection);
                    command.ExecuteNonQuery();
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
            }
            return _groupMember;
        
        }

        public void Update(GroupMember item)
        {
           try
            {
                connection.Open();
                bool valueAdded = false;
                string updateCommand = "UPDATE mailgroup SET (";
                string values = " values (";

                if (item.Key != null)
                {
                   return;
                }

                if (item.Name != null)
                {
                    if (valueAdded)
                    {
                        updateCommand += ", ";
                        values += ", ";
                    }
                    updateCommand += "mailgroup.name";
                    values += "\"" + item.Name + "\"";
                    valueAdded = true;
                }

                if (item.Recipient != null)
                {
                    if (valueAdded)
                    {
                        updateCommand += ", ";
                        values += ", ";
                    }
                    updateCommand += "mailgroup.recipient";
                    values += "\"" + item.Recipient + "\"";
                    valueAdded = true;
                }

                updateCommand += ")";
                values += ")";

                MySqlCommand command = new MySqlCommand(updateCommand + values + " where mailgroup  .key=" + item.Key + ");", connection);

                command.ExecuteNonQuery();
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }
    }
}