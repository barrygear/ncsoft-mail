/*
 * Project: NCSoft-Mail
 * File: src/Repository/MailUserRepository_Mysql.cs
 */
using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace Mail
{
    public class MailUserRepository_Mysql : IMailUserRepository
    {
        MySqlConnection connection;

        public MailUserRepository_Mysql()
        {                
            connection = new MySqlConnection
            {
                //ConnectionString = "server=localhost;uid=root;persistsecurityinfo=False;port=3306;SslMode=None;database=ncsoft"
                ConnectionString = ($"{Startup.Configuration["AppConfiguration:ConnectionString"]}")
            };
        }

        public IEnumerable<MailUser> GetAll()
        {
            List<MailUser> _mailUsers = new List<MailUser>();

            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("SELECT mailuser.id, mailuser.key, mailuser.name, " + 
                "mailuser.recipient FROM mailuser;", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int _id = reader.GetInt16(0);
                        string _key = reader.GetString(1);
                        string _name = reader.GetString(2);
                        string _recipient = reader.GetString(3);
        
                        _mailUsers.Add(new MailUser { 
                            Id = _id, 
                            Key = _key, 
                            Name = _name, 
                            Recipient = _recipient});
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return _mailUsers;
        }

        public MailUser FindByRecipient(string recipient)
        {
            MailUser _mailUser = null;

            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("SELECT mailuser.id, mailuser.key, mailuser.name," + 
                " mailuser.recipient FROM mailuser where mailuser.recipient=\"" + recipient + 
                "\" order by mailuser.id;", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    if(reader.HasRows) {
                        reader.Read();
                        
                        int _id = reader.GetInt16(0);
                        string _key = reader.GetString(1);
                        string _name = reader.GetString(2);
                        string _recipient = reader.GetString(3);
                
                        _mailUser = new MailUser {               
                            Id = _id, 
                            Key = _key, 
                            Name = _name, 
                            Recipient = _recipient };
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return _mailUser;
        }

        public MailUser FindByName(string name)
         {
            MailUser _mailUser = null;

            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("SELECT mailuser.id, mailuser.key, mailuser.name," + 
                " mailuser.recipient FROM mailuser where mailuser.name=\"" + name + 
                "\" order by mailuser.id limit 1;", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    if(reader.HasRows) {
                        reader.Read();

                        int _id = reader.GetInt16(0);
                        string _key = reader.GetString(1);
                        string _name = reader.GetString(2);
                        string _recipient = reader.GetString(3);

                        _mailUser = (new MailUser { 
                            Id = _id, 
                            Key = _key, 
                            Name = _name, 
                            Recipient = _recipient });
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return _mailUser;
        }
        public void Add(MailUser item)
        {
            if(item.Key == null) {
                item.Key = Guid.NewGuid().ToString();
            }
            connection.Open();
            try
            {
                MySqlCommand command = new MySqlCommand("INSERT into mailuser (mailuser.key, mailuser.name, mailuser.recipient) values (\""
                    + item.Key
                    + "\", \""
                    + item.Name
                    + "\", \""
                    + item.Recipient
                    + "\");"
                    , connection);
                int result = command.ExecuteNonQuery();
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }

        public MailUser Find(string key)
        {
            MailUser _mailUser = null;

            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("SELECT mailuser.id, mailuser.key, mailuser.name, " + 
                "mailuser.recipient FROM mailuser where mailuser.key=\"" + key + "\" limit 1;", connection);

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    if(reader.HasRows) {
                        reader.Read();

                        int _id = reader.GetInt16(0);
                        string _key = reader.GetString(1);
                        string _name = reader.GetString(2);
                        string _recipient = reader.GetString(3);

                        _mailUser = (new MailUser { 
                            Id = _id, 
                            Key = _key, 
                            Name = _name, 
                            Recipient = _recipient });
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return _mailUser;
        }

        public MailUser Remove(string key)
        {
            MailUser _mailUser = null;
            try
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand("DELETE FROM mailuser where mailuser.key=\"" + key + "\" limit 1;", connection);
                command.ExecuteNonQuery();
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return _mailUser;
        }

        public void Update(MailUser item)
        {
            try
            {
                connection.Open();
                bool valueAdded = false;
                string updateCommand = "UPDATE mailuser SET (";
                string values = " values (";

                if (item.Key != null)
                {
                   return;
                }

                if (item.Name != null)
                {
                    if (valueAdded)
                    {
                        updateCommand += ", ";
                        values += ", ";
                    }
                    updateCommand += "mailuser.name";
                    values += "\"" + item.Name + "\"";
                    valueAdded = true;
                }

                if (item.Recipient != null)
                {
                    if (valueAdded)
                    {
                        updateCommand += ", ";
                        values += ", ";
                    }
                    updateCommand += "mailuser.recipient";
                    values += "\"" + item.Recipient + "\"";
                    valueAdded = true;
                }

                updateCommand += ")";
                values += ")";

                MySqlCommand command = new MySqlCommand(updateCommand + values + " where mailuser.key=" + item.Key + ");", connection);

                command.ExecuteNonQuery();
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }
    }
}