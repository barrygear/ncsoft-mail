# NCSoft Mail application

## Building

1. You need the .NET Core 1.1 SDK installed. see https://www.microsoft.com/net/download/core#/current
1. In your shell run ```dotnet restore && dotnet build``` in the directory containing the downloaded source. 

### If you have building issues, check to see if the directory is named ```NCSoft-Mail```. Git clones typically pull it down in lower case and .net wants it in the original mixed case.

At this point, it should be runnable using : ```dotnet run```

## Packaging

To create a stand alone application (for the current architecture):

1. ```dotnet restore && dotnet build```
1. ```dotnet package -c Release```

This creates a 'package' in bin/Release/netcoreapp1.1/<RTI>/publish where RTI could be win7-x64 or osx.10.12-x64. Zip up 
this contents and copy it to destination.

To build for a specific platform other than the current one, add -r <RTI> to the build and publish commands where the RTI is 
debian.8-x64, centos.7-x64, osx.10.12-x64, win7-x64, or win10-x64. For example:

1. ```dotnet restore && dotnet build -r win7-x64```
1. ```dotnet package -c Release -r win7-x64```

## Testing

To run unit tests:

1. ```dotnet restore && dotnet test```

## Configuring

Data Store options
There are 3 backends available.

1. Mem - In memory hashmap, not persistable.
1. NoDB - Saves data to json files. Data persists between runs. See https://github.com/joeaudette/NoDb
1. MySQL - MySQL backend. Requires setup.


MySQL/MariaDB setup:

```mysql < ncsoft_mail.sql```

## Settings

The appsettings.json file contains the MySQL config string and the current datastore type. The default looks like:

```JavaScript
{
    "AppConfiguration": {
        "ConnectionString": "server=localhost;uid=root;persistsecurityinfo=False;port=3306;SslMode=None;database=ncsoft",
        "DataStore": "NoDB"
    }
}
```

Values for DataStore are :

1. ```mem```
1. ```NoDB``` - Default value
1. ```Mysql```

For information on configuring the mysql connection string see https://dev.mysql.com/doc/connector-net/en/connector-net-programming-connecting-connection-string.html

The unit tests are set to use in memory hashtables by default. It can be switched to NoDB, however switching to MySQL will produce an error since xunit doesn't support
Dependency Injection. The configuration string from the appsettings.json is made avaiable via DI, and it will break. You can provide a Fixture class for xunit that will configure
mysql, however, its too restricted to hard coded connection strings. I think NoDB or mem is sufficient for unit testing. To change to NoDB, change the TestStartup to use "NoDB" at lines 32 and 33.
Its a manual process since, in normal conditions, unit tests would be fixed to a single datastore anyway.

I have ran the unit tests against all 3 backends successfully (I manually set the mysql connection string for the unit tests).

## Ruminations

* Unfortunately I had to spend a lions share of the time on getting the core app functional. Its lacking enough documentation details to consider it a 'learning/teaching' application.
* While I was comfortable and familiar with C#, that consisted mostly of creating Unity 3D plugins writing code that bridged a C/C++ library with C#, and a subset of C# as well. Working with a stand alone REST service forced me to spend a large amount of time on workflow and tooling rather than implementing the service.
* The mysql implementation is in sore need of a database ObjectRelationalManager. While writing raw sql is instructional and somewhat clear in intention, its too much of a management time sink at scale and is prone to SQL overflow attacks.
* XUnit is a nice unit test framework focusing on running tests in parallel. Unfortunately its not Dependency Injection compatible which is at odds with .net core design. To compensate the offer a Fixture system that allows you to manually construct and 'Inject' objects into test code. I started to experiment with as few and decided its a little beyond scope to fully implement however I left the initial classes.
* The primary unit test is MailTest_TestPlan.cs which is a test plan conversion I rapidly did to test example usage flows.
* The is a MailTest.cs class with I was going to fill out with positive and negative unit tests for the various repositories but I focussed on the TestPlan code.
* The REST flow I implemented is one that focuses on using HTTP response codes as result values. Other REST implementations from major companies use the raw HTTP response codes to represent only server or proxy errors and returns in a format (usually JSON) that contains an inner error code that represents the REST calls error state. While this is offers a wider range of error codes and a separation from server state vs application state, I believe it forces the user to take extra steps to implement the data wrapper and error code handling. Since this is a simple REST service, I stuck with the base HTTP response codes.
* HTTPClient doesn't do PATCH so I had to add it.
* XUnit requires additional code to force unit test ordering. To do so, you create a class that decided what tests are a lower or higher prioity and provide a new attribute for XUnit to pass to that class. Those are the PriorityOrderer.cs and the TestPriorityAttribute.cs classes. To specifiy an order you add ```[TestCaseOrderer("MailTests.PriorityOrderer", "NCSoft-Mail")]``` to the top of the test class and a new attribute ```TestPriorty(level)```  to the [Fact] or [Theory] statement.
* I added a simple Docker builder ```build-docker.sh```. This will build and deploy a dockerized version of the NCSoft-Mail service (assuming you have docker installed locally). To launch you user ```docker run -it ncsoft-mail``` and use ctrl-c to quit. Since the default datastore is NoDB, which preserves the mail in the filesystem, you will need to map an external directory to its datastore path (nodb_storage) if you wish to preserve the data in between runs. While it worked when I initially set it up, I've ran into some docker issues and have shelved it.
* I wanted to integrate Swagger to autogenerate REST documentation as well as include a way to test the services but its not fully compatible with .net core 1.1 yet. I left code commented out in the Startup class. Apparently a fork of Swagger called Ahoy is what they are using to stabilize it. The current process is to build Ahoy yourself and include it. Again, I didnt think I had the time to do so.
* I added a cURL script that runs various commands demonstrating the api's usage. The output isn't pretty though.