FROM microsoft/dotnet:runtime

WORKDIR /dotnetapp
#COPY project.json .
#RUN dotnet restore
EXPOSE 5000
# copy and build everything else
COPY  bin/Release/netcoreapp1.1/debian.8-x64 /dotnetapp
#RUN dotnet publish -c Release -o out
ENTRYPOINT ["publish/NCSoft-Mail"]

