#
# SQL Export
# Created by Querious (1061)
# Created: December 12, 2016 at 11:37:29 PM PST
# Encoding: Unicode (UTF-8)
#


CREATE DATABASE IF NOT EXISTS `ncsoft` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
USE `ncsoft`;




SET @PREVIOUS_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS;
SET FOREIGN_KEY_CHECKS = 0;


DROP TABLE IF EXISTS `mailuser`;
DROP TABLE IF EXISTS `mailgroup`;
DROP TABLE IF EXISTS `mail`;


CREATE TABLE `mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` varchar(128) DEFAULT NULL,
  `key` varchar(128) NOT NULL DEFAULT '',
  `read` tinyint(1) DEFAULT '0',
  `message` varchar(256) DEFAULT NULL,
  `subject` varchar(128) DEFAULT NULL,
  `recipient` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mail_idx_recipient` (`recipient`) USING BTREE,
  KEY `mail_idx_sender` (`sender`) USING BTREE,
  KEY `mail_idx_key` (`key`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;


CREATE TABLE `mailgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(128) NOT NULL DEFAULT '',
  `name` varchar(128) DEFAULT NULL,
  `recipient` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mailgroup_idx_key` (`key`) USING BTREE,
  KEY `mailgroup_idx_name` (`name`) USING BTREE,
  KEY `mailgroup_idx_recipient` (`recipient`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;


CREATE TABLE `mailuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(128) NOT NULL DEFAULT '',
  `name` varchar(128) DEFAULT NULL,
  `recipient` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mailuser_idx_key` (`key`) USING BTREE,
  KEY `mailuser_idx_name` (`name`) USING BTREE,
  KEY `mailuser_idx_recipient` (`recipient`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
